//require(moduleHelper.js)

const API = "api/v4";
const CLIENT_ID = "436ff92354ce2dce97ec15334e709788199dff0a6e06856a9b89c367734f6f55";
const MODULE_NAME = "fileProvider";
let server = document.getElementById("remoteServer").value;

document.getElementById("btnLogin").addEventListener("click",e => {
    server = document.getElementById("remoteServer").value;
    let w = window.open( `https://${server}/oauth/authorize?client_id=${CLIENT_ID}&redirect_uri=${window.location.href}&response_type=token&state=YOUR_UNIQUE_STATE_HASH`,'_blank');
    w.focus();
});

document.getElementById("btnRepoSelection").addEventListener("click",e => {
    getBranchesInProject();
});

let token = location.hash.substr(14).split("&")[0];

let bc = new BroadcastChannel('token_channel');

if(token) {
    bc.postMessage(token);
    window.close();
}
else if(sessionStorage.getItem("token")) {
    token = sessionStorage.getItem("token");
    getUser();
    getUserProjects();
}

function getUser() {
    let oReq = new XMLHttpRequest();
    oReq.addEventListener("load", function(){console.log(this.responseText)});
    oReq.open("GET", `https://${server}/${API}/user`);
    oReq.setRequestHeader("Authorization", `Bearer ${token}`);
    oReq.send();
}

function getUserProjects() {
    let oReq = new XMLHttpRequest();
    oReq.addEventListener("load", function(){console.log(this.responseText)});
    oReq.open("GET", `https://${server}/${API}/projects?simple=true&owned=true`);
    oReq.setRequestHeader("Authorization", `Bearer ${token}`);
    oReq.send();
    let oReq2 = new XMLHttpRequest();
    oReq2.addEventListener("load", function(){console.log(this.responseText)});
    oReq2.open("GET", `https://${server}/${API}/projects?simple=true&owned=false&membership=true`);
    oReq2.setRequestHeader("Authorization", `Bearer ${token}`);
    oReq2.send();
}


bc.onmessage = function (e) { 
    token = e.data;
    sessionStorage.setItem("token",token);
 }

function getBranchesInProject() {
    let server = document.getElementById("remoteServer").value;
    let projectID = document.getElementById("inputProjectID").value;
    let oReq = new XMLHttpRequest();
    oReq.addEventListener("load", getBranchesInProjectListener);
    oReq.open("GET", `https://${server}/${API}/projects/${projectID}/repository/branches`);
    oReq.setRequestHeader("Authorization", `Bearer ${token}`);
    oReq.send();
}

function getBranchesInProjectListener() {
    let select = document.getElementById("selectBranch");
    while (select.firstChild) {
        select.removeChild(select.firstChild);
    }
    let option = document.createElement("option");
    option.value = "--select branch--";
    option.text = "--select branch--";
    select.appendChild(option);
    JSON.parse(this.responseText).forEach(branch => {
        let option = document.createElement("option");
        option.value = branch.name;
        option.text = branch.name;
        select.appendChild(option);
    });
}

let selectBranch = document.getElementById("selectBranch");
selectBranch.addEventListener("change", e => {
    if(selectBranch.firstChild.value === "--select branch--")
        selectBranch.removeChild(selectBranch.firstChild);
    emitEvent("fileProviderReady",{branch:selectBranch.value});
});


cmds["getFile"] = getFile;
function getFile(m) {
    let server = document.getElementById("remoteServer").value;
    let projectID = document.getElementById("inputProjectID").value;
    let branch = document.getElementById("selectBranch").value;
    let oReq = new XMLHttpRequest();
    oReq.addEventListener("load", function() {
        getFileCallback(m,this.responseText);
    });
    oReq.open("GET", `https://${server}/${API}/projects/${projectID}/repository/files/${encodeURIComponent(m.file_path)}?ref=${branch}`);
    oReq.send();
}

function getFileCallback(m,res) {
    //might to pass more info about file, ie last modified...
    m.callback.data = {content:b64DecodeUnicode(JSON.parse(res).content)};
    messageModule(m.callback,m.callback.target);
}

cmds["getFilesInDirectory"] = getFilesInDirectory;
function getFilesInDirectory(m) {
    let path = m.path;
    let server = document.getElementById("remoteServer").value;
    let branch = document.getElementById("selectBranch").value;
    let projectID = document.getElementById("inputProjectID").value;
    let oReq = new XMLHttpRequest();
    oReq.addEventListener("load", function(){getFilesInDirectoryListener(m,container,this.responseText);});
    oReq.open("GET", `https://${server}/${API}/projects/${projectID}/repository/tree?ref=${branch}${path ? "&path="+path: ""}`);
    oReq.setRequestHeader("Authorization", `Bearer ${token}`);
    oReq.send();
}

let fileList = [];

function getFilesInDirectoryListener (m,container,json) {
    let files = JSON.parse(json);
    files.forEach(f => {
        fileList.push(f);
        f.originalPath = f.path;
    });
    m.callback.data = {files:files};
    messageModule(m.callback,m.callback.target);
}



cmds["commit"] = commit;
function commit(m) {
    let payload = JSON.stringify({
        "branch": m.branch || document.getElementById("selectBranch").value,
        "commit_message": m.commit_message,
        "actions": m.actions
    });
    console.log(payload);
    
    let server = document.getElementById("remoteServer").value;
    let projectID = document.getElementById("inputProjectID").value;
    let oReq = new XMLHttpRequest();
    commitTime = Date.now();
    oReq.addEventListener("load", function(){emitEvent("confirmCommit",{res:this.responseText,commitNumber:m.commitNumber,doMergeRequest:m.doMergeRequest});});
    oReq.addEventListener("error", e => emitEvent("commitError",{error:e,commitNumber:m.commitNumber}));
    oReq.open("POST", `https://${server}/${API}/projects/${projectID}/repository/commits`);
    oReq.setRequestHeader("Authorization", `Bearer ${token}`);
    oReq.setRequestHeader("Content-Type","application/json");
    oReq.send(payload);
}















//from https://developer.mozilla.org/en-US/docs/Web/API/WindowBase64/Base64_encoding_and_decoding
function b64DecodeUnicode(str) {
    return decodeURIComponent(Array.prototype.map.call(atob(str), function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2)
    }).join(''))
}